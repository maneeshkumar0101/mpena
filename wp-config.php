<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mpena' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mn6X#%-<iL%=_~j-4W%S[)Bf|<%XL(>{s>.9D^jHexq*$rRGAZ#vsO_rcQXcfm[4' );
define( 'SECURE_AUTH_KEY',  ',aeCfLPSH,G8-pXOA3Pv5CKzJ1/N9qIyRefzRLn/DWZs.Ki,8#1038^@6/euZbh8' );
define( 'LOGGED_IN_KEY',    'tb>kZEBE7kG;/uJi[HtRj4H9qvY|!K4[;[@Gd@n^a8E<-{AF[&t+C0[F|04IjOIo' );
define( 'NONCE_KEY',        '=gNStNlRl!gYTnbfK;w YgKHCY+dO0zT3`XG!-HwwXBI!p%:IFU =>DT&|0}@!:{' );
define( 'AUTH_SALT',        '^jI,4a&6+Ao`G{L1;F$34t!srG2Rb-ee<^lvoIyo4AJP*;{DnEwS^hc6or#s+3AG' );
define( 'SECURE_AUTH_SALT', 'm:d6mf*OTW8PcZZit[LM_MpkNodpv6Z.ng:m;BF;T4Gq`xi4zu,0rSMM,xw1CQ^Y' );
define( 'LOGGED_IN_SALT',   'B>8%)|.*|wV/gGoSZ ipjAQ7`1PAK-<;!_J]F3|wOO(E9WTWf:CvDL(5&8N*|K~P' );
define( 'NONCE_SALT',       '*mC[rA#j#pMwo;ym j)IMAI%z0&ds$IWrkXdBwBZm~iGZ$FCTTQK@:6sRT?2(:Hq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
