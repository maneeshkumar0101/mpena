<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



function svp_block_type($myBlockName, $svp_BlockOption = array()) {
	register_block_type(
		'svp-kit/' . $myBlockName,
		array_merge(
			array(
				'editor_script' => 'svp-kit-editor-script',
				'editor_style' => 'svp-kit-editor-style',
				'script' => 'svp-kit-front-script',
				'style' => 'svp-kit-front-style'
			),
			$svp_BlockOption
		)
	);
}

function svp_blocks_script() {
	wp_register_script(
		'svp-kit-editor-script',
		plugins_url('dist/js/editor-script.js', __FILE__),
		array(
			'wp-blocks',
			'wp-i18n',
			'wp-element',
			'wp-editor',
			'wp-components',
			'wp-compose',
			'wp-data',
			'wp-autop',
		)
	);	
	svp_block_type('kahf-banner-k27f', array(
		'render_callback' => 'svp_block_custom_post_fun',
		'attributes' => array(
			'postName' => array(	
				'type' => 'string',
				'source' => 'html',
			),
		)
	));
	
}
add_action('init', 'svp_blocks_script');



function svp_block_custom_post_fun ( $attributes, $content ) {
	
	return wpautop( $content );
}