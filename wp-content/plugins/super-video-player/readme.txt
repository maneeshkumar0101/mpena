=== Super Video Player - Best wordpress video player ===
Contributors: abuhayat
Tags: Video Player, mp4 Player, video gallery, Media Player, Plyr
Donate link: https://gum.co/wpdonate
Requires at least: 3.1
Tested up to: 5.4
Stable tag: 1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A very simple, accessible and fully customisable Video player plugin that support .mp4 .ogg and subtitle.

== Description ==

Super Video Player is a lite weight, fully customizable self hosted video player plugin that support mp4 / ogg as well as multiple caption tracks or subtitle file. 

== Demo ==
 [Click Here To see demo ](https://1infosoft.com/super-video-player-demo/ "See demo")  

= Video Tutorial =

https://www.youtube.com/watch?v=LJym2Pe1h2k

= Features =
* User friendly interface
* Make the player look how you want with the markup you want
* Full support for VTT captions and screen readers.
* Support multiple subtitle file for multiple language.
* The video player is compact so it does not take a lot of real estate on your webpage
* HTML5 compatible so the video files embedded with this plugin will play on iOS devices
* Works on all major browsers -Edge, IE7, IE8, IE9, Safari, Firefox, Chrome
* The video player is responsive. Thats means it works with any screen size
* Player can be used to embed the video files on your WordPress posts or pages
* If you are selling video files from your site then you can use this plugin to offer a preview
* Add the video player to any post/page using shortcode
* Use autoplay option to play an video file as soon as the page loads
* You can play unlimited video
* Support picture-in-picture mode
* Powered by html5 



= How to use  =
- After install you can see a sidebar menu in the dashboare called "Super Video Player"
- Add one or more Videos from Here.
- You will get Shortcode for every Video In The Editor Screen And Video Lists.
- Copy Shortcode 
- Past the shortcode in post, page, widget areas To publish them. if you want to publish a player in template file use <?php echo do_shortcode('SHORTCODE') ?>
- Enjoy !

= Gutenberg Block =
- This plugin add a Gutenberg Block Called "Super Video Player" Under Common Category 
- Go to your WordPress Admin interface and open a post or page editor.
- Click the plus button in the top left corner or in the body of the post/page.
- Search or See in Common Block Category and select Super Video Player.
- Click the Icon to add it.
- Select A Video from dropdown List  
- Publish and Enjoy ! 



== Installation ==

This section describes how to install the plugin and get it working.

E.g

1. Upload `plugin-directory` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use shortcode in page, post or in widgets.
4. If you want video in your theme php file, Place `<?php echo do_shortcode('YOUR_SHORTCODE'); ?>` in your templates


== Frequently Asked Questions ==

= How do I install this plugin? =

You can install as others regular wordpress plugin. No different way. Please see on installation tab.
= Can i have to pay? =

No, Its completely free.

= What Video type can i play? =

You can play mp4, ogg video file.

= How many player i can publish in my site? =

You can publish unlimited video with unlimited player.



== Screenshots ==

1. The player in frontend
2. Sidebar Menu
3. UI
4. Shortcode

== Changelog ==

= 1.0 =
* Initial Release

= 1.1 =
* Fix Multiplayer issue.
* Added How to use Article
* Improved Performance

= 1.2 =
* Fix an issue. 
* add support for external source

= 1.3 =
* Add support for Gutenberg Block
