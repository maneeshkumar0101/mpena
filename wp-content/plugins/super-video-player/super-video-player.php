<?php 
/*
 * Plugin Name: Super Video Player
 * Plugin URI:  https://abuhayatpolash.com/super-video-player-demo/
 * Description: A lite weight completely customizable self hosted video player that support mp4 / ogg and subtitle
 * Version: 1.2
 * Author: Abu Hayat Polash
 * Author URI: http://abuhayatpolash.com
 * Text Domain:  super-video-player
 * Domain Path:  /languages
 */

function svp_load_textdomain() {
    load_plugin_textdomain( 'super-video-player', false, dirname( __FILE__ ) . "/languages" );
}

add_action( "plugins_loaded", 'svp_load_textdomain' );

 /*Some Set-up*/
define('SVP_PLUGIN_DIR', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' ); 
define('SVP_FREE','yes' ); 


// Gumroad script
function svp_gumroad_script(  ) {

    wp_enqueue_script( 'my_custom_script', 'https://gumroad.com/js/gumroad-embed.js', array(), '1.0' );
}
add_action( 'admin_enqueue_scripts', 'svp_gumroad_script' );


/* Latest jQuery of wordpress */
if ( ! function_exists( 'svp_add_jquery' ) ) :
function svp_add_jquery() {
	wp_enqueue_script('jquery');
}
add_action('init', 'svp_add_jquery');
endif;


/* JS*/
if ( ! function_exists( 'svp_get_script' ) ) :
function svp_get_script(){    
    wp_enqueue_script( 'svp-js', plugin_dir_url( __FILE__ ) . 'js/plyr.js', array('jquery'), '20120206', false );
}
add_action('wp_enqueue_scripts', 'svp_get_script');
endif;



function svp_style() {
    wp_enqueue_style( 'svp-style', plugin_dir_url( __FILE__ ) . 'css/player-style-custom.css' );

}
add_action( 'wp_enqueue_scripts', 'svp_style' );

/*-------------------------------------------------------------------------------*/
/* HIDE everything in PUBLISH metabox except Move to Trash & PUBLISH button
/*-------------------------------------------------------------------------------*/

function svp_hide_publishing_actions(){
        $my_post_type = 'svplayer';
        global $post;
        if($post->post_type == $my_post_type){
            echo '
                <style type="text/css">
                    #misc-publishing-actions,
                    #minor-publishing-actions{
                        display:none;
                    }
                </style>
            ';
        }
}
add_action('admin_head-post.php', 'svp_hide_publishing_actions');
add_action('admin_head-post-new.php', 'svp_hide_publishing_actions');
	



//Remove post update massage and link 
function svp_updated_messages( $messages ) {
    $messages['svplayer'][1] = __('Updated');
    return $messages;
}
add_filter('post_updated_messages','svp_updated_messages');
 
// Inc  Metabox
		
include_once('metabox/meta-box-class/my-meta-box-class.php');
include_once('metabox/class-usage-demo.php');
include_once('gutenblock/index.php');

 

/* Register Custom Post Types */  
            add_action( 'init', 'svp_create_post_type' );
            function svp_create_post_type() {
                    register_post_type( 'svplayer',
                            array(
                                    'labels' => array(
                                            'name' => __( 'Super Video Player'),
                                            'singular_name' => __( 'Player' ),
                                            'add_new' => __( 'Add New' ),
                                            'add_new_item' => __( 'Add new item' ),
                                            'edit_item' => __( 'Edit' ),
                                            'new_item' => __( 'New' ),
                                            'view_item' => __( 'View' ),
											'search_items'       => __( 'Search'),
                                            'not_found' => __( 'Sorry, we couldn\'t find any item you are looking for.' )
                                    ),
                            'public' => false,
							'show_ui' => true, 									
                            'publicly_queryable' => true,
                            'exclude_from_search' => true,
                            'show_in_rest' => true,
                            'menu_position' => 14,
							'menu_icon' =>SVP_PLUGIN_DIR .'img/icon.png',
                            'has_archive' => false,
                            'hierarchical' => false,
                            'capability_type' => 'page',
                            'rewrite' => array( 'slug' => 'svplayer' ),
                            'supports' => array( 'title','thumbonail' )
                            )
                    );
            }	
			
// ONLY OUR CUSTOM TYPE POSTS
add_filter('manage_svplayer_posts_columns', 'svp_column_handler', 10);
add_action('manage_svplayer_posts_custom_column', 'svp_column_content_handler', 10, 2);
 
// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function svp_column_handler($defaults) {
    $defaults['directors_name'] = 'ShortCode';
    return $defaults;
}
function svp_column_content_handler($column_name, $post_ID) {
    if ($column_name == 'directors_name') {
        // show content of 'directors_name' column
		echo '<input onClick="this.select();" value="[vplayer id='. $post_ID . ']" >';
    }
}
			
// Review Request as admin notice

function svp_review_request_metabox() {


	add_meta_box(
		'myplugin_sectionid',
		__( 'Please show some love', 'myplugin_textdomain' ),
		'svp_review_callback',
		'svplayer',
		'side'
	);
}
add_action( 'add_meta_boxes', 'svp_review_request_metabox' );

function svp_review_callback( ) {echo'

<p>If you like <strong>Super Video Player</strong> Plugin, please leave us a <a href="https://wordpress.org/support/plugin/super-video-player/reviews/?filter=5#new-post" target="_blank">&#9733;&#9733;&#9733;&#9733;&#9733; rating</a> . Your Review is very important to us as it helps us to grow more.</p>

<p>Not happy, Sorry for that. You can request for improvement. </p>

<table>
	<tr>
		<td><a class="button button-primary button-large" href="https://wordpress.org/support/plugin/super-video-player/reviews/?filter=5#new-post" target="_blank">Write Review</a></td>
		<td><a class="button button-primary button-large" href="mailto:abuhayat.du@gmail.com" target="_blank">Request Improvement</a></td>
	</tr>
</table>

'; };

function svp_review_request_notice() {
    ?>
    <div class="notice notice-success is-dismissible">
        <h2>Please show some love </h2>
		<p><?php $url = 'https://wordpress.org/support/plugin/super-video-player/reviews/?filter=5#new-post';
			$text = sprintf( __( 'If you like <strong>Super Video Player</strong> please leave us a <a href="%s" target="_blank">&#9733;&#9733;&#9733;&#9733;&#9733;</a> rating. Your Review is very important to us as it helps us to grow more. <strong>Need some improvement ? </strong> We like to listen from you ! <a href="mailto:abuhayat.du@gmail.com">Request for improvement.</a>', 'svp-review' ), $url ); echo $text; ?></p>
    </div>
    <?php
}
//add_action( 'admin_notices', 'svp_review_request_notice' );			

// Footer Review Request 

	add_filter( 'admin_footer_text','svp_admin_footer');	 
	function svp_admin_footer( $text ) {
		if ( 'svplayer' == get_post_type() ) {
			$url = 'https://wordpress.org/support/plugin/super-video-player/reviews/?filter=5#new-post';
			$text = sprintf( __( 'If you like <strong>Super Video Player</strong> please leave us a <a href="%s" target="_blank">&#9733;&#9733;&#9733;&#9733;&#9733;</a> rating. Your Review is very important to us as it helps us to grow more. ', 'post-carousel' ), $url );
		}

		return $text;
	}
	
	//  Developer page
add_action('admin_menu', 'svp_developer_page');

function svp_developer_page() {
	add_submenu_page( 'edit.php?post_type=svplayer', 'Developer', 'Developer', 'manage_options', 'svp-developer', 'svp_developer_page_callback' );
}

function svp_developer_page_callback() {
	
	echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		echo '<h2>Developer</h2>
		<h2>Md Abu hayat polash</h2>
		<h3>Professional Web Developer</h3>
		<p>Hire Me : <a target="_blank" href="https://www.upwork.com/freelancers/~01c73e1e24504a195e">On Upwork.com</a>
		<p>Email: <a href="mailto:abuhayat.du@gmail.com">abuhayat.du@gmail.com </a></p>
		<p>Skype: ah_polash</p>
		
		
		';
	echo '</div>';

}

//How to use
add_action('admin_menu', 'svp_howto_page');

function svp_howto_page() {
	add_submenu_page( 'edit.php?post_type=svplayer', 'How to use', 'How to use', 'manage_options', 'svp_howto', 'svp_howto_page_callback' );
}

function svp_howto_page_callback() {
	
	echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		echo '<h2>How to use </h2>
		<h2>Watch the video to learn how to use the plugin. </h2>
		<br/>
		<iframe width="789" height="375" src="https://www.youtube.com/embed/LJym2Pe1h2k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<br />
		
		';
	echo '</div>';

}

// Check For Pro addons

add_action( 'admin_init', 'svp_check_addons_plugin' );
function svp_check_addons_plugin() {
  if ( is_plugin_active('super-video-player-pro-addons/super-video-player-pro-addons.php' ) ) {
	
	//include_once('metabox.php');

	}else
	{ 
		// Add functions for ads and Pro buy

function spv_add_metabox() {
	add_meta_box(
		'donation',
		__( 'Support Super Video Player', 'myplugin_textdomain' ),
		'svp_free_callback_donation',
		'svplayer'
	);	
	add_meta_box(
		'myplugin_sectionid',
		__( 'Try Our LightBox Plugin', 'myplugin_textdomain' ),
		'svp_free_addons_callback',
		'svplayer',
		'side'
	);
	add_meta_box(
		'myplugin',
		__( 'Please show some love', 'myplugin_textdomain' ),
		'svp_free_callback',
		'svplayer',
		'side'
	);		
}
add_action( 'add_meta_boxes', 'spv_add_metabox' );
function svp_free_callback_donation( ) {echo '

<div class="gumroad-product-embed" data-gumroad-product-id="wpsvp" data-outbound-embed="true"><a href="https://gumroad.com/l/wpsvp">Loading...</a></div>
';};
function svp_free_addons_callback(){echo'<a target="_blank" href="http://bit.ly/2GiuI2G"><img style="width:100%" src="'.SVP_PLUGIN_DIR.'/img/lightbox.png" ></a>
<p>LightBox Plugin Enabled you to Open a Video Player in a beautiful lightBox. </p>
<table>
	<tr>
		<td><a class="button button-primary button-large" href="http://bit.ly/2lTcGgE" target="_blank">See Demo </a></td>
		<td><a class="button button-primary button-large" href="http://bit.ly/2GiuI2G" target="_blank">Buy Now</a></td>
	</tr>
</table>
';};

function svp_free_callback( ) {echo 'If you like <strong>Super Video Player</strong> Plugin, please leave us a <a href="https://wordpress.org/support/plugin/super-video-player/reviews/?filter=5#new-post" target="_blank">&#9733;&#9733;&#9733;&#9733;&#9733; rating.</a> Your Review is very important to us as it helps us to grow more.
<p>Need some improvement ? <a href="mailto:abuhayat.du@gmail.com">Please let us know </a> how can we improve the Plugin.</p>';};

// Dashboard widget
function svp_add_dashboard_widgets() {
 	wp_add_dashboard_widget( 'svp_dashboard_widget', 'Support Super Video Player', 'svp_dashboard_widget_function' );
 
 	global $wp_meta_boxes;
 	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
 	$example_widget_backup = array( 'svp_dashboard_widget' => $normal_dashboard['svp_dashboard_widget'] );
 	unset( $normal_dashboard['svp_dashboard_widget'] );
	$sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );
 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
} 

function svp_dashboard_widget_function() {

	// Display whatever it is you want to show.
	echo '<div class="uds-box" data-id="1"></div>';
	echo '<p>It is hard to continue development and support for this plugin without contributions from users like you. If you enjoy using the plugin and find it useful, please consider support by <b>DONATION</b> Or <b>BUY THE PRO VERSION (NO ADS)</b> of the Plugin. Your support will help encourage and support the plugins continued development and better user support.</p>
	
<center>
<a target="_blank" href="https://gum.co/wpdonate"><div><img width="200" src="'.SVP_PLUGIN_DIR.'img/donation.png'.'" alt="Donate Now" /></div></a>


<h2>Buy It Now to GET 40% Off !- Only for 50 Customers - Use Code : First50 </h2>
</center>	
<div class="gumroad-product-embed" data-gumroad-product-id="wpsvp" data-outbound-embed="true"><a href="https://gumroad.com/l/wpsvp/first50">Loading...</a></div>

';}
add_action( 'wp_dashboard_setup', 'svp_add_dashboard_widgets' );
	
  }
}
	
/*-------------------------------------------------------------------------------*/
/* Lets register our shortcode
/*-------------------------------------------------------------------------------*/
if (!defined('SVP_PRO')) :
function svp_shortcode_func($atts){
	extract( shortcode_atts( array(

		'id' => null,

	), $atts ) ); 

?>
<?php ob_start();?>
<div style="<?php $pwidth=get_post_meta($id,'_svp_width', true); if ($pwidth==0){echo 'width:100%';}else{echo 'max-width:'.$pwidth.'px';} ?>">
<!--    Free  -->
 <video controls playsinline class="player<?php echo $id; ?>" <?php $status1= get_post_meta($id,'_svp_video_repeat', true); if ($status1=="loop"){echo "loop";}?> <?php $stutas= get_post_meta($id,'_svp_video_muted', true); if ($stutas=="on"){echo"muted ";} ?><?php $stutas= get_post_meta($id,'_svp_video_autoplay', true); if ($stutas=="on"){echo" autoplay ";}?><?php $poster=get_post_meta($id,'_svp_video_poster', true); if(!empty($poster)) { echo'poster="'. $poster['url'].'"';} ?>>
 <source src="<?php $video=get_post_meta($id,'_svp_video_file', true); echo $video['url'];?>" type="video/mp4">
  Your browser does not support the video tag.
  
<?php $subtitle=get_post_meta($id,'_svp_re_', true); if(!empty($subtitle)){
	 foreach($subtitle as $subtitledata){ ?>
	<track kind="captions" label="<?php echo $subtitledata['_svp_label'];?>" src="<?php echo $subtitledata['_svp_sub_id']['url'];?>" srclang="en" default>
	 <?php }} ?>
	 
</video>

<script type="text/javascript">

const players<?php echo $id;?> = Plyr.setup('.player<?php echo $id;?>', {
	fullscreen:{ enabled: true, fallback: true, iosNative: true },

});
</script>


</div>
<?php $output = ob_get_clean();return $output;//print $output; // debug ?>

<?php
}
add_shortcode('vplayer','svp_shortcode_func');	
endif;		
// Add shortcode area 	

add_action('edit_form_after_title','svp_shortcode_area');
function svp_shortcode_area(){
global $post;	
if($post->post_type=='svplayer'){
?>	
<div>
	<label style="cursor: pointer;font-size: 13px; font-style: italic;" for="svp_shortcode">Copy this shortcode and paste it into your post, page, or text widget content:</label>
	<span style="display: block; margin: 5px 0; background:#1e8cbe; ">
		<input type="text" id="svp_shortcode" style="font-size: 15px; border: none; box-shadow: none;padding: 4px 8px; width:100%; background:transparent; color:white;"  onfocus="this.select();" readonly="readonly"  value="[vplayer id=<?php echo $post->ID; ?>]" /> 
		
	</span>
</div>
 <?php   
}}