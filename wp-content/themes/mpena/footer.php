<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
<?php 

 	if(is_front_page()){
 ?>
			<footer id="contact">
				 <div class="footer-top">
				 <div class="container">
				 <div class="row">
				 <div class="col-md-3">
				 <h3>Contacts</h3>
				 <ul>
				 <li>Chandigarh, India
				</li>
				  <li><i class="fa fa-phone" aria-hidden="true"></i>8080000000</li>
				   <li><i class="fa fa-envelope" aria-hidden="true"></i>realdategroup@gmail.com</li>
				    <li></li>
				 </ul>
				 
				 
				 </div>
				<div class="col-md-3">
				 <h3>Popular</h3>
				 <ul>
				 <?php
    	 				//wp_nav_menu( array( 'theme_location' => 'primary' ) );
   
                        $menuLocations = get_nav_menu_locations();

                        //echo '<pre>'; print_r($menuLocations);
                         // Get our nav locations (set in our theme, usually functions.php)
                                           // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

                        $menuID = $menuLocations['primary']; // Get the *primary* menu ID

                        $primaryNav = wp_get_nav_menu_items($menuID);
                        //echo '<pre>'; print_r($primaryNav); die;
                        foreach ( $primaryNav as $navItem ) {

                        echo '<li class="nav-item active" ><a id="item-id"  href="'.$navItem->url.'" title="'.$navItem->title.'">'.$navItem->title.'</a></li>';

                        }
                    ?>
				   
				 </ul>
				 
				 </div>
				 <div class="col-md-3">
				 <h3>Service</h3>
				 <ul>
				 <li><a href="http://mpenatwe.com/privacy">Privacy & term </a></li>
				  
				   
				 </ul>
				 
				 </div>
				 
				 <div class="col-md-3 ">
				 <h3>Get The App</h3>
				 <ul>

				 <?php $meta = get_post_meta($post->ID, 'playstore', true); ?>

				 <li><a href="<?php echo $meta; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/google.png"></a></li>
				 
												
				   
				 </ul>
				 
				 </div>
						
				 </div>
				 </div>
				 </div>
				 <p class="footer-bottom-text">© 2020 RealAsianDate, All rights reserved</p>
				 </footer>
				 
				 
				 
				 










				<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.js"></script>

				<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.js"></script>
				<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.js"></script>
				<script>
				$(document).ready(function(){
				$('.slider_img').slick({
				  dots: true,
				  arrows:false,
				  autoplay:true,
				  autoplaySpeed:1000,
				  
				 
				  slidesToShow: 1
				 });
				});</script>
				




		<?php wp_footer(); ?>

	</body>
</html>
<?php }else { ?>

	<section >

<div class="foot-nav" >
<nav class="navbar navbar-expand-lg   navbar-fixed-bottom" style="background-color:#bdb6b5;">
  <a class="navbar-brand" style="color: rgba(255,255,255,1);font-family: Montserrat;
		font-style: normal;">© 2020 RealAsianDate, All rights reserved</a>
  

  
    <ul class="nav navbar-nav ml-auto ml">
      
    				<li class="nav-item active1">
        			<a class="nav-link" href="<?php echo home_url();?>/" style="text-decoration:none;">Home <span class="sr-only">(current)</span></a>
      				</li>
      				<li class="nav-item active2">
       				 <a class="nav-link" href="<?php echo home_url();?>/blog/" style="text-decoration:none;">Blog</a>
      				</li>
                    <li class="nav-item active3 " ><a id="item-id" class="nav-link" href="<?php echo home_url();?>/?#contact ">Contact Us </a></li>
    </ul>
   
  
</nav>
</div>
</section>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.js"></script>
</body>
</html>
<?php } ?>