<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/read.css">
<div class="container">
<div class="row">
<div class="col-md-7 box">
<div class="photo">

		<?php
						the_post_thumbnail('full', array( 'class'  => 'responsive-class img-fluid' ) );

						$caption = get_the_post_thumbnail_caption();

							if ( $caption ) {
					?>

				<figcaption class="wp-caption-text"><?php echo esc_html( $caption ); ?></figcaption>

				<?php
							}
				?>


</div>

<h1><?php the_title(); ?></h1>


		


<?php
						if ( is_search() || ! is_singular()) {
							
							the_excerpt();
						} else {
							the_content( __( 'Continue reading', 'twentytwenty' ) );
						}
						?>


<br><br><br>
<div class="form1">
<div class="row" style="width;100%">
<?php

	/**
	 *  Output comments wrapper if it's a post, or if comments are open,
	 * or if there's a comment number – and check for password.
	 * */
	if ( ( is_single() || is_page() ) && ( comments_open() || get_comments_number() ) && ! post_password_required() ) {
		?>

		<div class="comments-wrapper section-inner">

			<?php comments_template(); ?>

		</div><!-- .comments-wrapper -->

		<?php
	}
	?>
</div>
</div>
</div>
<?php 
 global $wpdb;
        $table = 'wp_posts';
if(isset($_POST['submit'])){
	
	$like = $_POST['my_name'];

	//echo $like; die('hiii');
	$query = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title  like '%" . $like . "%'" );
	//print_r($query);die('hiii');
	$postLocation = get_permalink($query);
	$total_rows = $wpdb->num_rows;
	if($total_rows>0)
			{
	
	//print_r($postLocation);die;
	//print_r($query);die('hiii');
	?><script>
                window.location = "<?php echo $postLocation;
                 ?>";
                //alert("thanks for join us");
             </script><?php 
         }else{  

  				global $wp_session;
      		   $wp_session['message'] = 'This Blog Not Available';

         		?><script>
               // window.location = "<?php //echo $postLocation?; ?>";
                //alert("thanks for join us");
             </script><?php

         }
	} ?>



<div class="col-md-5 box1">

 <form class="form-inline my-2 my-lg-0" action=""  method="post">
      <input  class="form-control mr-sm-2" name="my_name" type="search" placeholder="Search" aria-label="Search" required>
	  <div class="bnn1">
      <button  name="submit" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button></div>
    </form>
	<div style="color:red;">
		<?php echo $wp_session['message']; 
		wp_destroy_current_session(); ?>
	</div>
	<h5>Recent Blog</h5>
	<?php 
   // the query
   $the_query = new WP_Query( array(
     
      'posts_per_page' => 3,
   		)); 
	?>

	<?php if ( $the_query->have_posts() ) : ?>
  	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

		<a style="color:blue;" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
		<?php the_title(); ?>
		
		</a><br>
             

  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>

  <?php else : ?>
  		<p><?php __('No Blogs'); ?></p>
  <?php endif; ?>
	
	
	
	
</div>
</div>
</div><!-- .post -->





