<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
 ?>

<?php 
?>

	
					
				<div class="col-md-4" >
				<div class="card">

				<div class="inner" id="post-<?php the_ID(); ?>">
				<a href="<?php the_permalink() ?>">
        			<?php
						the_post_thumbnail('full', array( 'class'  => 'responsive-class img-fluid' ) );

						$caption = get_the_post_thumbnail_caption();

							if ( $caption ) {
					?>

				<figcaption class="wp-caption-text"><?php echo esc_html( $caption ); ?></figcaption>

				<?php
							}
				?>
                



                
    			</a>   
    			 </div>

				<div class="card-body">
				<?php if ( is_singular() ) {
								the_title( '<h class="h4 mt-4 col-12">', '</h>' );
							} else {
								the_title( '<h class="h4 mt-4 col-12" ><a href="' . esc_url( get_permalink() ) . '" style="color:#2e486f;">', '</a></h>' );
							}
				
				?> 				
					<?php
						if ( is_search() || ! is_singular()) {
							
							the_excerpt();
						} else {
							the_content( __( 'Continue reading', 'twentytwenty' ) );
						}
						?>
							
				

				<a href="<?php the_permalink() ?>"><button class="btn read"> Read More<i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>
				</div>
				</div><br><br>
				</div>
					
				
				


	<?php

	//get_template_part( 'template-parts/entry-header' );

	if ( ! is_search() ) {
		//get_template_part( 'template-parts/featured-image' );
	}

	?>

	<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">

		<div class="entry-content">

			<?php
			//if ( is_search() || ! is_singular() && 'summary' === get_theme_mod( 'blog_content', 'full' ) ) {
				//the_excerpt();
			//} else {
				//the_content( __( 'Continue reading', 'twentytwenty' ) );
			//}
			?>

		</div><!-- .entry-content -->

	</div><!-- .post-inner -->

	<div class="section-inner">
		<?php
		wp_link_pages(
			array(
				'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'twentytwenty' ) . '"><span class="label">' . __( 'Pages:', 'twentytwenty' ) . '</span>',
				'after'       => '</nav>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			)
		);

		//edit_post_link();

		// Single bottom post meta.
		twentytwenty_the_post_meta( get_the_ID(), 'single-bottom' );

		if ( post_type_supports( get_post_type( get_the_ID() ), 'author' ) && is_single() ) {

			get_template_part( 'template-parts/entry-author-bio' );

		}
		?>

	</div><!-- .section-inner -->

	<?php

	if ( is_single() ) {

		get_template_part( 'template-parts/navigation' );

	}

	/**
	 *  Output comments wrapper if it's a post, or if comments are open,
	 * or if there's a comment number – and check for password.
	 * */
	if ( ( is_single() || is_page() ) && ( comments_open() || get_comments_number() ) && ! post_password_required() ) {
		?>

		<div class="comments-wrapper section-inner">

			<?php comments_template(); ?>

		</div><!-- .comments-wrapper -->

		<?php
	}
	?>

				
			