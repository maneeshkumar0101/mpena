<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta http-equiv="X-UA-compatible" content="IE=edge">
<title>Real Asian Date</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style3.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/responsive1.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/print.css">

<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">

<style>

html {
    margin-top: 0px !important;
}

html,body{
width:100%;
overflow-x:hidden;
list-style:none;
text-decoration:none;


}
.navbar span{
color:#fff;}
button,input{
color:#fff;}
</style>

<?php wp_head(); ?>

</head>
<body id="home">

	<?php 
	if(is_singular()  && !is_front_page()){ ?>

		<nav class="navbar navbar-expand-lg   navbar-fixed-top" style="background-image:linear-gradient(to right, #4a4584, #9c5573);">
  <a class="navbar-brand" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/logo1.svg"></a>
  <button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span><i class="fa fa-bars" aria-hidden="true"></i></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="nav navbar-nav ml-auto ml">
    	 <?php
    	 				//wp_nav_menu( array( 'theme_location' => 'primary' ) );
   
                        $menuLocations = get_nav_menu_locations();

                        //echo '<pre>'; print_r($menuLocations);
                         // Get our nav locations (set in our theme, usually functions.php)
                                           // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

                        $menuID = $menuLocations['primary']; // Get the *primary* menu ID

                        $primaryNav = wp_get_nav_menu_items($menuID);
                        //echo '<pre>'; print_r($primaryNav); die;
                        foreach ( $primaryNav as $navItem ) {

                        echo '<li class="nav-item active" ><a id="item-id" class="nav-link" href="'.$navItem->url.'" title="'.$navItem->title.'">'.$navItem->title.'</a></li>';

                        }
                    ?>
    	
           <li class="nav-item " ><a id="item-id" class="nav-link" href="<?php echo home_url();?>/?#contact ">Contact Us </a></li>
    </ul>
   
  </div>
</nav>

<?php	} else {
	?>

<header class="top-head">
		
<nav class="navbar navbar-expand-lg   navbar-fixed-top">
  <a class="navbar-brand" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/logo1.svg"></a>
  <button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span><i class="fa fa-bars" aria-hidden="true"></i></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="nav navbar-nav ml-auto ml">
    	 <?php
    	 				//wp_nav_menu( array( 'theme_location' => 'primary' ) );
   
                        $menuLocations = get_nav_menu_locations();

                        //echo '<pre>'; print_r($menuLocations);
                         // Get our nav locations (set in our theme, usually functions.php)
                                           // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

                        $menuID = $menuLocations['primary']; // Get the *primary* menu ID

                        $primaryNav = wp_get_nav_menu_items($menuID);
                        //echo '<pre>'; print_r($primaryNav); die;
                        foreach ( $primaryNav as $navItem ) {

                        echo '<li class="nav-item active" ><a id="item-id" class="nav-link" href="'.$navItem->url.'" title="'.$navItem->title.'">'.$navItem->title.'</a></li>';

                        }
                    ?>
    	
      <li class="nav-item " ><a id="item-id" class="nav-link" href="<?php echo home_url();?>/?#contact ">Contact Us </a></li>
    </ul>
   
  </div>
</nav>

<section class="banner">

<div class="logo">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/logo1.svg">
</div>
<?php $header_file = get_post_meta($post->ID, 'header_title', true); ?>
<div class="caption">
 <?php echo $header_file; ?> </div>

 <?php 

 	if(is_front_page()){
 ?>
	
 <span id="error" style="color:red;"></span>	
<?php //echo do_shortcode("[kento_email_subscriber_campaign id=107]"); ?>
 <?php //saveEmailAndPhone(); ?> 

<form method="post" action="<?php //echo get_home_url(); ?>" class="d-flex flex-row from">
		<div class="btn1"> 
    <input type="text" name="name"  class="btn px-4 py-3 mr-5 bg-white" placeholder="Email" style="font-family: DM Serif Display;
    font-style: normal;
    font-weight: normal;" autocomplete="off" required> </div>
  <div class="btn2">
  <input type="submit" id="show" name="submit" value="Notify Me" class="btn site-btn1 px-4 py-3 mr-4" style="background-color:#5224ad;color: rgba(255,255,255,1); font-family: Montserrat;
  font-weight: bold;
    font-style: normal;
    font-weight: normal;">
		</div>
</form>  
<?php

if (isset($_GET['message'])) {
    ?> <div style="color:green;
    font-size: 23px;"> <?php
    echo $_GET['message'];
    ?> </div> <?php
}else{  ?><div style="color:red;
    font-size: 20px;"> <?php
    echo $_GET['warning'];
    ?> </div> <?php }  ?>
		
		<h2>COMING SOON</h2>
		<?php $meta = get_post_meta($post->ID, 'playstore', true); 
     //print_r($meta);
?>
		<center>
		<div class="bn2" >
		<a href="<?php echo $meta; ?>"><img class="mybtn" src="<?php echo get_stylesheet_directory_uri(); ?>/logo/btn2.png" class="px-4 py-3 "></a>
		</div></center>	
		
		<div class="arrow">
	
		<a href="#all"><img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/arrow.svg"></a></div>
  
<?php } ?>

</section>
</header>
 <?php } ?> 
