<?php
/**
 * Template Name: Home
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();


   

?>   


 	<section id="all">
		
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
the_content();
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>


<section>
<div class="container">
<div class="how">
<h2>How Does It Work?</h2>
</div>
</div>
</section>



	</section> 
 
 
		 <section >
		<div class="container-fluid">
			<div class="row main_banner1">
				<div class="col-md-5 sub-banner">
				<div class="photo1">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/MatchTwo.png" style="height:350px;transform: rotate(-7deg);">
				</div>
				 <div class="photo2">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/Matchone.png" style="height:350px;transform: rotate(-1deg);">
				 </div>
				 </div>
				<div class="col-md-5 banner2">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/Better & Honest Matches.svg">
						
						<h2>Better & Honest Matches</h2>
						<p>Meet 100 % Approved Real<br> Asians</p>
				</div>
			</div>

		</div></section>
 
 
 
						<section>
	<div class="container-fluid">
	<div class="row  main_banner3">
	<div class="col-md-5 sub-banner2">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/likes.svg">
	<h1>More Likes</h1>
	<p>Know your Visitors & Who Interests in you

</p>
	</div>
	<div class="col-md-5 photo">
	<div class="photo1">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/MatchTwo.png" style="height:350px;transform: rotate(-5deg);">
	</div> 
	<div class="photo2">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/MoreLikes.png" style="height:350px; transform: rotate(2.5deg);">
	</div>
	</div>
	</div>
	</div>
	</section>
 
 <section>
		<div class="container-fluid">
			<div class="row main_banner4">
				<div class="col-md-5 sub">
				<div class="photo1">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/Registration.png" style="height:350px;transform: rotate(-6deg);">
				</div>
				 <div class="photo2">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/ProfileRegistration.png" style="height:350px;">
				 </div>
				 </div>
				<div class="col-md-7 banner3">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/Easy_registeration_vector.svg">
						
						<h1>Easy Registration</h1>
						<p>Build strong profiles<br>quickly

</p>
				</div>
			</div>

		</div></section>
 
 
 <section>
	<div class="container-fluid">
	<div class="row  main_banner5">
	<div class="col-md-5 sub-banner3">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/message.svg">
	<h1>Great Conversations</h1>
	<p>Start messaging quickly after joining

</p>
	</div>
	<div class="col-md-5 photo2">
	<div class="photo1">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/Messagetwo.png" style="height:350px;transform: rotate(-3deg);">
	</div>
	<div class="photo2">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/Messageone.png" style="height:350px;width:190px;transform: rotate(2deg);">
	</div>
	</div>
	</div>
	</div>
	</section>
	<section>
	
	
	<section>
		<div class="container-fluid">
			<div class="row main_banner10">
				<div class="col-md-5 sub10">
				<div class="photo1">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/se.png" style="height:350px;width:190px;transform: rotate(-11deg);">
				</div> 
				 <div class="photo2">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/search.png" style="height:350px;width:190px;">
				 </div>
				 </div>
				<div class="col-md-7 banner10">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/Deep_search_vector.svg">
						<h1>Deep Search</h1>
						<p>Find dates based on<br> effective filters

</p>
				</div>
			</div>

		</div></section>
 
  <section class="lastone">
	<div class="container-fluid">
	<div class="row  main_banner5">
	<div class="col-md-5 sub-banner3">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/video_stories_vector.svg">
	<h1>Video Stories</h1>
	<p>Watch and upload video <br>stories


</p>
	</div>
	<div class="col-md-5 photo2">
	<div class="photo1">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/ProfileRegistration.png" style="height:350px;transform: rotate(-3.0deg);">
	</div> 
	<div class="photo2">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/logo/Video Feeds.png" style="height:350px;width:190px;transform: rotate(2.0deg);">
	</div>
	</div>
	</div>
	</div>
	</section>
 
 

 
 
 <section>
	<div class="container-fluid">
	<div class="main_banner7">
	<div class="slider_img">
  <div>
  	<?php echo do_shortcode('[metaslider id="138"]'); ?>
  </div>
</div>
	
	</div>
	</div>
	
	</section>
 
 
 
 <br>



<?php 



get_footer(); ?>
