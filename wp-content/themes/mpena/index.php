<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
?> <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/blog.css"><?php
get_header();


//$cat = the_category_ID();


//print_r ($cat->cat_name); die;
//echo $mk =$category->cat_name;

//if(has_category($cat= 'home')){

?>

<section>
	<div class="container" >
				<div class="row part1" >
	

	<?php

	$archive_title    = '';
	$archive_subtitle = '';

	if ( is_search() ) {
		global $wp_query;

		$archive_title = sprintf(
			'%1$s %2$s',
			'<span class="color-accent">' . __( 'Search:', 'twentytwenty' ) . '</span>',
			'&ldquo;' . get_search_query() . '&rdquo;'
		);

		if ( $wp_query->found_posts ) {
			$archive_subtitle = sprintf(
				/* translators: %s: Number of search results. */
				_n(
					'We found %s result for your search.',
					'We found %s results for your search.',
					$wp_query->found_posts,
					'twentytwenty'
				),
				number_format_i18n( $wp_query->found_posts )
			);
		} else {
			$archive_subtitle = __( 'We could not find any results for your search. You can give it another try through the search form below.', 'twentytwenty' );
		}
	} elseif ( ! is_home() ) {
		$archive_title    = get_the_archive_title();
		$archive_subtitle = get_the_archive_description();
	}

	if ( $archive_title || $archive_subtitle ) {
		?>

		<header class="archive-header has-text-align-center header-footer-group">

			<div class="archive-header-inner section-inner medium">

				<?php if ( $archive_title ) { ?>
					<h1 class="archive-title"><?php echo wp_kses_post( $archive_title ); ?></h1>
				<?php } ?>

				<?php if ( $archive_subtitle ) { ?>
					<div class="archive-subtitle section-inner thin max-percentage intro-text"><?php echo wp_kses_post( wpautop( $archive_subtitle ) ); ?></div>
				<?php } ?>

			</div><!-- .archive-header-inner -->

		</header><!-- .archive-header -->

		<?php
	}

	if ( have_posts() ) {

		$i = 0;

		while ( have_posts() ) {
			$i++;
			if ( $i > 3 ) { ?>

		<?php	}
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		}
	} elseif ( is_search() ) {
		?>

		<div class="no-search-results-form section-inner thin">

			<?php
			get_search_form(
				array(
					'label' => __( 'search again', 'twentytwenty' ),
				)
			);
			?>

		</div><!-- .no-search-results -->

		<?php
	}
	?>

	<?php get_template_part( 'template-parts/pagination' ); ?>
</div>
</div> 
</section><!-- #site-content -->
<section>
<div class="container part4">
<div class="Heading">
<h1>Real Asian Date </h1></div>
<div class="pera">
<p>Owning a hair or beauty salon is great fun and very rewarding. But at the same time, it can also be hard work and tough at times. It becomes vital for owners and managers to stay updated with the latest trends.

The salon industry is evolving at a great pace. With new products and techniques, it is equally vital to promote your salon online. Social media is a powerful tool for promoting your salon business and developing long-lasting relationships with your customers. For a salon business, you need to have a great relationship with your clients. It is one of the major factors of success. From following up with customers, to booking appointments and offering discounts, you need to be active everywhere. 

Using social media for building your relationship with clients and providing valuable content is a powerful way to promote your salon and strengthen these relationships. If you’re running a salon, you want to stand out from the crowd in your area. As a result, you want to get more customers for your salon. 

The salon industry is expected to go digital and in order to keep your salon competitive, you’ll need to know the hottest marketing trends. New changes happen every now and then, and hence it is important to stay updated on the trends this dynamic industry has in-store.</p>
</div>
</div>


</section>
<section class="part5">
<div class="container">
<div class="Heading">
<h1>Real Asian Date </h1></div>
<div class="pera">
<p>Owning a hair or beauty salon is great fun and very rewarding. But at the same time, it can also be hard work and tough at times. It becomes vital for owners and managers to stay updated with the latest trends.

The salon industry is evolving at a great pace. With new products and techniques, it is equally vital to promote your salon online. Social media is a powerful tool for promoting your salon business and developing long-lasting relationships with your customers. For a salon business, you need to have a great relationship with your clients. It is one of the major factors of success. From following up with customers, to booking appointments and offering discounts, you need to be active everywhere. 

Using social media for building your relationship with clients and providing valuable content is a powerful way to promote your salon and strengthen these relationships. If you’re running a salon, you want to stand out from the crowd in your area. As a result, you want to get more customers for your salon. 

The salon industry is expected to go digital and in order to keep your salon competitive, you’ll need to know the hottest marketing trends. New changes happen every now and then, and hence it is important to stay updated on the trends this dynamic industry has in-store.</p>
</div></div>

</section>

<?php
get_footer();
